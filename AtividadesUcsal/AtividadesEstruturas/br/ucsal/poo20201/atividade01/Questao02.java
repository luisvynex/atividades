package br.ucsal.poo20201.atividade01;

import java.util.Scanner;

public class Questao02 {

	public static void main(String[] args) {
		Integer valor;
		Long e = 0L;
		Scanner sc = new Scanner(System.in);

		System.out.println("Escreva um n�mero inteiro e positivo");
		valor = sc.nextInt();

		for (int i = 0; i < valor; i++) {
			Long fat = 1L;
			for (int j = 0; j <= i; j++) {
				fat *= j;
			}
			e += 1L / fat;

		}

		sc.close();
	}

}
